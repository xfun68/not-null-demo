package com.twuc.webApp.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class OfficeJpaTest {
    @Autowired
    private OfficeRepository repo;

    @Autowired
    private EntityManager em;

    @Test
    void hello_world() {
        assertTrue(true);
    }

    @Test
    void should_save_an_entity() {
        Office savedOffice = repo.save(new Office(1L, "Xi'an"));
        em.flush();
        assertNotNull(savedOffice);
    }

    @Test
    void should_persist_entity_with_em() {
        em.persist(new Office(1L, "Xi'an"));
        em.flush();
        em.clear();
        Office office = em.find(Office.class, 1L);

        assertEquals(Long.valueOf(1), office.getId());
        assertEquals("Xi'an", office.getCity());
    }

    @Test
    void should_throw_exception_when_city_is_null_with_em() {
        assertThrows(PersistenceException.class, () -> {
            em.persist(new Office(1L, null));
            em.flush();
        });
    }

    @Test
    void should_throw_exception_when_city_is_null_with_repository() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            repo.save(new Office(1L, null));
            repo.flush();
            assertEquals(1, repo.findAll().size());
        });
    }
}
